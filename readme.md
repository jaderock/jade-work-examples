

# Work Examples by Jade
Apps, Wordpress websites and design work.

Project | Type | Description
--- | --- | --- | ---
Design | JPG/PNG | A collection of my design work
Blog | Laravel | An app to demonstrate CRUD functionality through the means of a blog with admin capabilities
Order Fulfilment System | Laravel & Vue.js | Allows the client to keep track of order progress and flags up overdue orders and pushes complete orders to the back of the list
Wordpress Barebones Theme | WordPress | A boilerplate for wordpress which includes typical JS libraries from external vendors, utilises ACF and custom shortcodes to make editing easier and more manageable for clients.
Blog | Laravel | An app to demonstrate CRUD functionality through the means of a blog 
Live Websites | WordPress | A collection of sites I have designed and built