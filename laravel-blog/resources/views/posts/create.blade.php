@extends('layouts.app')
@section('content')
<h1>Create Post</h1>
<form method="POST" action="{{ route('posts.store')}}">
        <div class="form-group">
            @csrf
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title"/>
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <input type="textarea" rows="4" class="form-control" name="body"/>
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
        <script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace( 'body' );
        </script>
    </form>
@endsection