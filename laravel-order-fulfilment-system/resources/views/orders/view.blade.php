@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Orders</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    @if (count($orders) || count($criticalOrders) || count($dispatchedOrders) || count($completeOrders))

                      @php
                          $currentDate = new DateTime(date('d-m-Y H:i:s'));
                      @endphp
                      {{--Table id is used by dataTables script in app.blade--}}
                      <table class="table" id="ordersTable">
                          <thead>
                              <tr>
                                  <th>Job No.</th>
                                  <th>Customer Name</th>
                                  <th>Due Date</th>
                                  <th>Status</th>
                              </tr>
                          </thead>
                          @foreach ($criticalOrders as $criticalOrder)

                              @php
                              //Convert the due date stored in the database back to a correctly formatted date for use on the front end (omits the time)
                              $criticalOrder->due_date = DateTime::createFromFormat('Y-m-d H:i:s', $criticalOrder->due_date)->format('d/m/Y')
                              @endphp

                              <tr id="{{$criticalOrder->id}}" onclick="window.location.href = '/orders/' + {{$criticalOrder->id}}" class="table-danger" data-amendable="false">
                                  <td>{{$criticalOrder->id}}</td>
                                  <td>{{$criticalOrder->customer_name}}</td>
                                  <td>{{$criticalOrder->due_date}}</td>
                                  <td>{{$criticalOrder->status}}</td>
                              </tr>

                          @endforeach

                          @foreach ($orders as $order)

                              @php
                              //Convert the due date stored in the database back to a correctly formatted date for use on the front end (omits the time)
                              $order->due_date = DateTime::createFromFormat('Y-m-d H:i:s', $order->due_date)->format('d/m/Y')
                              @endphp

                              <tr id="{{$order->id}}" onclick="window.location.href = '/orders/' + {{$order->id}}" data-amendable="true">
                                  <td>{{$order->id}}</td>
                                  <td>{{$order->customer_name}}</td>
                                  <td>{{$order->due_date}}</td>
                                  <td>{{$order->status}}</td>
                              </tr>

                          @endforeach

                          @foreach ($dispatchedOrders as $dispatchedOrder)

                              @php
                              //Convert the due date stored in the database back to a correctly formatted date for use on the front end (omits the time)
                              $dispatchedOrder->due_date = DateTime::createFromFormat('Y-m-d H:i:s', $dispatchedOrder->due_date)->format('d/m/Y')
                              @endphp

                              <tr id="{{$dispatchedOrder->id}}" onclick="window.location.href = '/orders/' + {{$dispatchedOrder->id}}" class="table-info" data-amendable="false">
                                  <td>{{$dispatchedOrder->id}}</td>
                                  <td>{{$dispatchedOrder->customer_name}}</td>
                                  <td>{{$dispatchedOrder->due_date}}</td>
                                  <td>{{$dispatchedOrder->status}}</td>
                              </tr>

                          @endforeach

                          @foreach ($completeOrders as $completeOrder)

                              @php
                              //Convert the due date stored in the database back to a correctly formatted date for use on the front end (omits the time)
                              $completeOrder->due_date = DateTime::createFromFormat('Y-m-d H:i:s', $completeOrder->due_date)->format('d/m/Y')
                              @endphp

                              <tr id="{{$completeOrder->id}}" onclick="window.location.href = '/orders/' + {{$completeOrder->id}}" class="table-success" data-amendable="false">
                                  <td>{{$completeOrder->id}}</td>
                                  <td>{{$completeOrder->customer_name}}</td>
                                  <td>{{$completeOrder->due_date}}</td>
                                  <td>{{$completeOrder->status}}</td>
                              </tr>

                          @endforeach

                      </table>
                    @else
                        No orders
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
  $('#ordersTable').DataTable({
    //Remove word search from search label, add placeholder txt
    "language": {
      "search": "",
      "searchPlaceholder": "Search"
    },
    "order": [],
    "dom": 'Btprf',
    "buttons": [
      'print'
    ],
    "columnDefs": [
        { "targets": [0, 1], "searchable": true, "orderable": true },
        { "targets": [2, 3], "searchable": false, "orderable": true }
    ],
    "iDisplayLength": 25,
    "rowCallback": function( row, data ) {
        if ($(row).data('amendable') === true) {
          if (data[3] == 'New Order') {
            $(row).addClass('table-tertiary');
         } else if (data[3] == 'In Workshop') {
            $(row).addClass('table-warning');
         }
        }
    }
  });
  //Hide default DataTables buttons and setup nav bar link to trigger print
  $('.dt-buttons').addClass('d-none');
  $('.jquery-print').click(function() {
    $('.dt-buttons .buttons-print').click();
  });
  //Move Datatables search to div within the nav
  $('.dataTables_filter').appendTo(".search-container");
});
</script>
@endsection
