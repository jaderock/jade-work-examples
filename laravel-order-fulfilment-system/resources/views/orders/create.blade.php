@extends('layouts.app')
@section('content')
@include('inc.print')
<div class="container" id="orders">
    <form action="{{ isset($order) ? route('orders.update', $order->id) : route('orders.store') }}" method="POST" class="needs-validation" novalidate>
        @csrf {{-- Include CSRF protection --}}
        @if (isset($order))
            @method('PUT')
        @endif

        <div class="row justify-content-center">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success d-print-none" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card" id="card-order-details">
                    @include('inc.order-details')
                </div>
            </div>
        </div>
        <div class="row justify-content-center item-container">
            <div class="col-12">
                @include('inc.item')
            </div>
        </div>
        <div class="row d-print-none">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-secondary mr-2 float-right d-print-none" v-on:click="addNewItem">+ Add Item</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('inc.grand-total')
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@if (isset($order))
<div class="modal fade" id="deleteOrder" tabindex="-1" role="dialog" aria-labelledby="deleteOrderTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteOrderTitle">Delete Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this order?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <form action="{{ route('orders.destroy', $order->id) }}" method="POST">
            @csrf {{-- Include CSRF protection --}}
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endif
<div class="modal fade" id="deleteFile" tabindex="-1" role="dialog" aria-labelledby="deleteFileTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteOrderTitle">Delete Attached File</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete the attached file?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" id="delete-file-confirm-button" data-dismiss="modal">Delete</button>
      </div>
    </div>
  </div>
</div>
<div id="fileUploadSection" class="d-none d-print-none">
  <form method="post" id="upload_form" enctype="multipart/form-data">
    @csrf {{-- Include CSRF protection --}}
    <input type="file" id="file-to-upload" name="file-to-upload" accept=".ai,.bmp,.eps,.gif,.jpg,.jpeg,.pdf,.png,.psd">
  </form>
</div>
<script type="text/javascript">
   $(function () {
       $('#due-date').datetimepicker({
           format: 'DD/MM/YYYY',
       });
       $('#order-date').datetimepicker({
           format: 'DD/MM/YYYY',
       });
       $('#dispatch-date').datetimepicker({
           format: 'DD/MM/YYYY',
       });
       $('#status').on('change', function(){
          if(this.value == 'Dispatched-Invoice' && !$('#dispatch-date > input').val()){
             $('#dispatch-date > input').val('{{ date('d/m/Y') }}');
          }
       });
   });
</script>
<script>
    var item = new Vue({
        el: '#orders',
        computed: {
          total: function(){

            function getSum(total, num) {
              return total + parseFloat(num.subtotal);
            }

            var total = this.items.reduce( getSum, 0 );

            return total.toFixed(2);
          }
        },
        data: {
            items:[
                @if (isset($order->items))
                    @foreach ($order->items as $item)
                        {
                            size: `{!! $item->size !!}`,
                            material: '{{$item->material}}',
                            thickness: '{{$item->thickness}}',
                            infill: '{{$item->infill}}',
                            holes: '{{$item->holes}}',
                            quantity: {!!$item->quantity ? $item->quantity : "''" !!},
                            price: {!!$item->price ? $item->price . '.toFixed(2)' : "''" !!},
                            subtotal: ({{$item->quantity ? $item->quantity : 0}} * {{$item->price ? $item->price : 0}}).toFixed(2),
                            engraving: `{!! $item->engraving !!}`,
                            completed: `{!! $item->completed ? $item->completed : '' !!}`
                        },
                    @endforeach
                @else
                {
                    size: '',
                    material: '',
                    thickness: '',
                    infill: '',
                    holes: '',
                    quantity: '',
                    price: 0,
                    subtotal: 0,
                    engraving: '',
                    completed: ''
                }
                @endif
            ],
        },
        methods: {
            addNewItem: function () {
                this.items.push({
                    size: '',
                    material: '',
                    thickness: '',
                    infill: '',
                    holes: '',
                    quantity: '',
                    price: 0,
                    subtotal: 0,
                    engraving: '',
                    completed: ''
                })
            },
            deleteItem: function (index){
                this.items.splice(index, 1);
            },
            updateSubtotal: function (index){
               this.items[index].subtotal = (this.items[index].quantity * this.items[index].price).toFixed(2);
           }
        }
    })
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
$(document).ready(function() {
  $('.jquery-print').click(function() {
      window.print();
  });
  $("#file-to-upload").change(function() {
    $("#upload_form").submit();
  });
  $('#upload_form').on('submit', function(event){
    event.preventDefault();
    $.ajax({
      url:"{{ route('ajaxuploadfile.action') }}",
      method:"POST",
      data:new FormData(this),
      dataType:'JSON',
      contentType: false,
      cache: false,
      processData: false,
      success:function(data)
      {
        $('#attach-file-button').hide();
        $('#uploaded_file').html(data.uploaded_file);
        $('#attached_file').val(data.attached_file);
      }
    })
  });
  $('#delete-file-confirm-button').click(function() {
    $('#attached_file').val('');
    $('#uploaded_file').html('<label class="btn btn-secondary btn-file" for="file-to-upload" id="attach-file-button"><i class="fas fa-file-upload"></i> Attach File</label><p><small class="text-danger"><strong>Important:</strong> File will not be deleted until the order is updated</small><br><small><strong>Accepted file types:</strong> AI, BMP, EPS, GIF, JPG, JPEG, PDF, PNG, PSD - <strong>Max file size:</strong> 25MB</small></p>');
  });

  @if (Route::current()->getName() == 'orders.create') // We're only loading this on the Create Order page

  function getDueDate(orderDate){ // A function that we can recall when certain events change and pass in a date if required
      var orderDate = orderDate || moment(); // If a date is passed in, then we'll use that, if not then we'll use 'now'
      orderDate = moment(orderDate,'DD/MM/YYYY'); // Format the date using Moment.js

      var days = 7; // This refers to the number of days we wish to add to the original date

      while (days > 0) {
         date = orderDate.add(1, 'days'); // Add 1 day to the date
         if (orderDate.isoWeekday() !== 6 && orderDate.isoWeekday() !== 7) { // If it's not a weekend
            days -= 1; // Remove 1 from the count
         }
      }

      $('#due-date input').val(date.format('DD/MM/YYYY'));
  }

  getDueDate(); // Ran on page load

  $('#order-date input').on("change keyup paste", function(){
     getDueDate( $(this).val() );
  });

  $("#order-date").on("change.datetimepicker", function(){
     getDueDate( $('#order-date input').val() );
  });
  @endif
});
</script>
@endsection
