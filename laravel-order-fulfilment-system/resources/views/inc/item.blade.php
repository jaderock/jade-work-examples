<transition-group name="slide-fade"class="row no-gutters" tag="div">
    <div class="item col-12 ml-0" v-for="(item, index) in items" v-bind:key="item">
        <div class="card">
            <div class="card-body">
                <transition name="fade">
                    <div v-if="items.length > 1" class="row delete-item-button-container">
                        <div class="col">
                            <button type="button" class="btn btn-danger btn-sm float-right mb-2 d-print-none" data-toggle="modal" :data-target="'#deleteItem-' + index">x</button>
                        </div>
                    </div>
                </transition>
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="form-group col-6">
                                <label class="mb-1 font-weight-bold">Size</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="size" placeholder="Enter size" :name="'items[' + index + '][size]'" v-model="item.size">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="material" class="mb-1 font-weight-bold">Material</label>
                                <div class="input-group">
                                    <select class="custom-select" id="material" :name="'items[' + index + '][material]'" v-model="item.material">
                                        <option value="">Choose...</option>
                                        <option value="Brushed Stainless">Brushed Stainless</option>
                                        <option value="Polished Stainless">Polished Stainless</option>
                                        <option value="Brushed Brass">Brushed Brass</option>
                                        <option value="Polished Brass">Polished Brass</option>
                                        <option value="Bronze">Bronze</option>
                                        <option value="Aluminium">Aluminium</option>
                                        <option value="Free Issue">Free Issue</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="mb-1 font-weight-bold">Thickness</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="thickness" placeholder="Enter thickness" :name="'items[' + index + '][thickness]'" v-model="item.thickness">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label class="mb-1 font-weight-bold">Infill</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="infill" placeholder="Enter infill" :name="'items[' + index + '][infill]'" v-model="item.infill">
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="mb-1 font-weight-bold">Holes</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="holes" placeholder="Enter holes" :name="'items[' + index + '][holes]'" v-model="item.holes">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-4">
                                <label class="mb-1 font-weight-bold">Quantity</label>
                                <div class="input-group">
                                    <input type="number" min="0" step="1" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" id="quantity" placeholder="Enter quantity" v-on:change="updateSubtotal(index)" :name="'items[' + index + '][quantity]'" v-model="item.quantity">
                                    <div class="invalid-feedback">
                                        Please provide a valid quantity.
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-4">
                                <label class="mb-1 font-weight-bold">Price</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="currency">£</span>
                                    </div>
                                    <input type="number" min="0" max="999999" step="0.01" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="price" placeholder="Enter price" :name="'items[' + index + '][price]'" v-on:change="updateSubtotal(index)" v-model="item.price" aria-describedby="currency">
                                    <div class="invalid-feedback">
                                        Please provide a valid price.
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-4">
                                <label class="mb-1 font-weight-bold">Subtotal</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="currency">£</span>
                                    </div>
                                    <input type="number" readonly class="form-control" v-model="item.subtotal" aria-describedby="currency">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 pl-4">
                        <label class="mb-1 font-weight-bold">Engraving</label>
                        <div class="input-group">
                            <textarea class="form-control{{ $errors->has('engraving') ? ' is-invalid' : '' }}" rows="12" id="engraving" placeholder="Enter engraving details" :name="'items[' + index + '][engraving]'" v-model="item.engraving"></textarea>
                            <div class="invalid-feedback">
                                This is a required field.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row d-print-none">
                    <div class="col">
                        <div class="form-check float-right">
                            <label class="form-check-label bg-light p-2 rounded font-weight-bold" :for="'completed-' + index">
                                <input type="checkbox" class="mr-2" :id="'completed-' + index" :name="'items[' + index + '][completed]'" v-model="item.completed">

                                Item Completed
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal fade" :id="'deleteItem-' + index" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Delete Item</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to delete this item?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger" data-dismiss="modal" v-on:click="deleteItem(index)">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</transition-group>
