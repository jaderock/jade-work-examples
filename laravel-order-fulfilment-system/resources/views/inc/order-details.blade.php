@if (!isset($order))
    <div class="card-header">
        <h5 class="font-weight-bold text-dark mb-0">Create New Order</h5>
    </div>
@else
    <div class="card-header {{ isset($statusClass) ? $statusClass : "" }} {{ isset($order->due_date_warning) ? "bg-danger text-light" : "text-light"}}">
        <div class="row">
            <div class="col-3">
                <h5 class="font-weight-bold my-0">Edit Order</h5>
            </div>
            <div class="col-9 text-right">
                <h5 class="my-0 d-inline-block mr-4">Status: <span class="font-weight-bold">{{$order->status}}{{ isset($order->due_date_warning) ? " - Overdue" : ""}}</span></h5>
                <h5 class="my-0 d-inline-block">Order Number: <span class="font-weight-bold">{{$order->id}}</span></h5>
            </div>
        </div>
    </div>
@endif

<div class="card-body">
    @if ($errors->any())
        <div class="alert alert-danger d-print-none">
            <ul class="mb-0">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="form-group col-7 mb-0">
            <label for="customer_name" class="mb-1 font-weight-bold">Customer Name <span class="text-danger">*</span></label>
            <div class="input-group mb-4">
                <input type="text" class="form-control{{ $errors->has('customer_name') ? ' is-invalid' : '' }}" id="customer-name" placeholder="Enter name" name="customer_name" value="{{ isset($order->customer_name) ? $order->customer_name : old('customer_name') }}" maxlength="191" required>
                <div class="invalid-feedback">
                    This is a required field.
                </div>
            </div>
            <label for="notes" class="mb-1 font-weight-bold">Notes</label>
            <div class="input-group mb-4">
                <textarea class="form-control" rows="9" id="notes" placeholder="Enter notes" name="notes">{{ isset($order->notes) ? $order->notes : old('notes') }}</textarea>
            </div>
        </div>
        <div class="form-group col-5">
            <label for="order_date" class="mb-1 font-weight-bold">Order Date <span class="text-danger">*</span></label>
            <div class="input-group date mb-4" id="order-date" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input{{ $errors->has('order_date') ? ' is-invalid' : '' }}" placeholder="DD/MM/YYYY" value="{{ isset($order->order_date) ? $order->order_date : date('d/m/Y') }}" data-target="#order-date" name="order_date" required />
                <div class="input-group-append d-print-none" data-target="#order-date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                <div class="invalid-feedback">
                    Please provide a valid order date.
                </div>
            </div>
            <label for="due_date" class="mb-1 font-weight-bold">Due Date <span class="text-danger">*</span></label>
            <div class="input-group date mb-4" id="due-date" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input{{ $errors->has('due_date') ? ' is-invalid' : '' }}" placeholder="DD/MM/YYYY" value="{{ isset($order->due_date) ? $order->due_date : old('due_date') }}" data-target="#due-date" name="due_date" required />
                <div class="input-group-append d-print-none" data-target="#due-date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                <div class="invalid-feedback">
                    Please provide a valid due date.
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="customer_name" class="mb-1 font-weight-bold">Status</label>
                    <div class="input-group mb-4">
                        <select class="custom-select" id="status" name="status">
                            <option {{ (isset($order->status) && $order->status == "New Order") ? "Selected" : "" }} value="New Order">New Order</option>
                            <option {{ (isset($order->status) && $order->status == "In Workshop") ? "Selected" : "" }} value="In Workshop">In Workshop</option>
                            <option {{ (isset($order->status) && $order->status == "Dispatched-Invoice") ? "Selected" : "" }} value="Dispatched-Invoice">Dispatched-Invoice</option>
                            <option {{ (isset($order->status) && $order->status == "Complete") ? "Selected" : "" }} value="Complete">Complete</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label for="customer_name" class="mb-1 font-weight-bold">Purchase Order No.</label>
                    <div class="input-group mb-4">
                        <input type="text" class="form-control{{ $errors->has('purchase_order_number') ? ' is-invalid' : '' }}" id="purchase-order-number" placeholder="Enter number" name="purchase_order_number" value="{{ isset($order->purchase_order_number) ? $order->purchase_order_number : old('purchase_order_number') }}" maxlength="191">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-0 mb-4">
        <div class="form-group col-4 mt-0">
            <label for="dispatch_method" class="mb-1 font-weight-bold">Dispatch Method</label>
            <div class="input-group mb-4">
                <select class="custom-select" id="dispatch-method" name="dispatch_method">
                    <option value="">Choose...</option>
                    <option {{ (isset($order->dispatch_method) && $order->dispatch_method == "Royal Mail") ? "Selected" : "" }} value="Royal Mail">Royal Mail</option>
                    <option {{ (isset($order->dispatch_method) && $order->dispatch_method == "Courier") ? "Selected" : "" }} value="Courier">Courier</option>
                </select>
            </div>
        </div>
        <div class="form-group col-4 mt-0">
            <label class="mb-1 font-weight-bold">Carriage Cost</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="currency">£</span>
                </div>
                <input type="number" min="0" max="999999" step="0.01" class="form-control{{ $errors->has('carriage_cost') ? ' is-invalid' : '' }}" id="carriage-cost" name="carriage_cost" placeholder="Enter price" aria-describedby="currency" value="{{ isset($order->carriage_cost) ? $order->carriage_cost : old('carriage_cost') }}">
                <div class="invalid-feedback">
                    Please provide a valid price.
                </div>
            </div>
        </div>
        <div class="form-group col-4 mt-0">
            <label for="dispatch_date" class="mb-1 font-weight-bold">Dispatch Date</label>
            <div class="input-group date mb-4" id="dispatch-date" data-target-input="nearest">
                <input type="text" class="form-control datetimepicker-input{{ $errors->has('dispatch_date') ? ' is-invalid' : '' }}" placeholder="DD/MM/YYYY" value="{{ isset($order->dispatch_date) ? $order->dispatch_date : old('dispatch_date') }}" data-target="#dispatch-date" name="dispatch_date" />
                <div class="input-group-append d-print-none" data-target="#dispatch-date" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                <div class="invalid-feedback">
                    Please provide a valid dispatch date.
                </div>
            </div>
        </div>
    </div>
    <div class="row d-print-none">
        <div class="form-group col-12 mb-0">
            <label class="mb-1 font-weight-bold">Attached File</label>
            <div class="input-group mb-0">
                <div id="uploaded_file">
                    @isset($order->attached_file)
                        @if($order->attached_file != '')
                            @php
                            $attached_file = json_decode($order->attached_file);
                            @endphp
                        @endif
                    @endisset
                    {!! isset($attached_file) ? '<p><i class="far fa-file"></i> ' . $attached_file->original_filename . '</p><a href="/orders/files/' . $attached_file->storage_filename . '/' . $attached_file->original_filename . '" target="_blank"><button type="button" class="btn btn-secondary"><i class="far fa-download"></i> Download File</button></a> <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteFile"><i class="far fa-trash-alt"></i> Delete File</button>' : '<label class="btn btn-secondary btn-file" for="file-to-upload" id="attach-file-button"><i class="fas fa-file-upload"></i> Attach File</label><p class="mb-0"><small><strong>Accepted file types:</strong> AI, BMP, EPS, GIF, JPG, JPEG, PDF, PNG, PSD - <strong>Max file size:</strong> 25MB</small></p>' !!}
                </div>
                <input type="hidden" id="attached_file" name="attached_file" value="{{ isset($order->attached_file) ? $order->attached_file : old('attached_file') }}">
                <div class="invalid-feedback">
                    File upload could not be saved
                </div>
            </div>
        </div>
    </div>

</div>
