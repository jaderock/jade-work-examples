<div class="row">
    <div class="form-group col-6">
        <button type="submit" class="btn btn-primary d-print-none">{{ isset($order) ? "Update Order" : "Create Order" }}</button>
        @if (isset($order))
            <button type="button" class="btn btn-danger ml-3 d-print-none" data-toggle="modal" data-target="#deleteOrder">Delete Order</button>
        @endif
    </div>
    <div class="col-6 grand-total">
        <p><b>Grand Total:</b> £@{{total}}</p>
    </div>
</div>
