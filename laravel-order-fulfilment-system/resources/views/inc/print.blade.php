<div class="container print-header d-none d-print-block">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="row align-items-center">
                <div class="col-8">
                    <img class="print-logo" src="{{ asset('images/hockerill-engraving-logo.png') }}" alt="Logo"/>
                </div>
                <div class="col-4">
                    <p class="text-right">
                        Unit 7a Mendip Business Park<br/>
                        Rooksbridge<br/>
                        BS26 2UG<br/>
                        Tel: 01934 750042<br/>
                        info@hockerillengraving.co.uk<br/>
                        www.hockerillengraving.co.uk<br/>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
