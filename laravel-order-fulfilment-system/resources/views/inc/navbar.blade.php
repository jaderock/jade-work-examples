<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('images/hockerill-engraving-logo.png') }}" alt="Logo"/>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
              @guest
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">
                    <i class="fas fa-sign-in-alt"></i> {{ __('Login') }}
                  </a>
                </li>
                <li class="nav-item">
                  @if (Route::has('register'))
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                  @endif
                </li>
              @else
              {{--Dashboard Nav Items--}}
              @if (Request::is('/'))
              <li class="nav-item">
                <a href="{{ route('orders.create') }}" class="nav-link">
                    <i class="fas fa-plus-circle"></i> New Order
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link jquery-print">
                  <i class="fas fa-print"></i> Print
                </a>
              </li>
              <li class="nav-item">
                <div class="search-container">

                </div>
              </li>

            {{--Create Order Nav Items--}}
            @elseif (Request::is('orders/create'))
            <li class="nav-item">
              <a href="/" class="nav-link">
                <i class="fas fa-chevron-circle-left"></i> Back
              </a>
            </li>

            {{--Order Nav Items--}}
            @elseif (Request::is('orders/*'))
              <li class="nav-item">
                <a href="/" class="nav-link">
                  <i class="fas fa-chevron-circle-left"></i> Back
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link jquery-print">
                  <i class="fas fa-print"></i> Print
                </a>
              </li>
              <li class="nav-item">
                {{--Delete button uses modal code in view to perform it's duties--}}
                <a href="#" class="nav-link" data-toggle="modal" data-target="#deleteOrder">
                  <i class="fas fa-trash"></i> Delete
                </a>
              </li>

            @endif

                <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="fas fa-user"></i> {{--{{ Auth::user()->name }}--}} <span class="caret"></span>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </div>
                </li>
              @endguest
            </ul>
          </div>
        </div>
      </nav>
