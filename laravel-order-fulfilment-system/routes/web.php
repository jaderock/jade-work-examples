<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Homepage/Dashboard
Route::get('/', 'OrderController@index')->name('home');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// 'Orders' resource controller
Route::resource('orders', 'OrderController')->except([
  'index'
]); // Can add in other resource routes as an array if required ( use resources() )

Route::get('/orders', function () {
  return abort(404);
});

// Orders Attach Files Routes
Route::post('/orders/action', 'OrderController@action')->name('ajaxuploadfile.action');
Route::get('/orders/files/uploads/{storage_filename}/{original_filename}', 'OrderController@files');

// Order Cron Jobs
Route::get('/cron/delete_old_orders', 'OrderController@deleteOldOrders');
