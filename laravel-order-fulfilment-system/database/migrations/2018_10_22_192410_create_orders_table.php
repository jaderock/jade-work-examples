<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name');
            $table->string('contact_details')->nullable();
            $table->text('delivery_details')->nullable();
            $table->dateTime('order_date');
            $table->dateTime('due_date');
            $table->enum('status', ['New Order', 'Processing', 'Waiting Collection', 'Complete']);
            $table->enum('job_type', ['New Items', 'Updates', 'Mix']);
            $table->integer('created_by');
            $table->dateTime('created_at');
            $table->integer('updated_by');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
